'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import {IndexRoute, Route, Router} from 'react-router';
import '../css/app.scss';
import createBrowserHistory from 'history/lib/createBrowserHistory';

import Body from './components/body.jsx';
import Brainstorms from './components/brainstorm/brainstorms.jsx';
import BrainstormDetail from './components/brainstorm/brainstorm-detail.jsx';
import BrainstormList from './components/brainstorm/brainstorm-list.jsx';
import Home from './components/home.jsx';
import Login from './components/user/login.jsx';
import Registration from './components/user/registration.jsx';

class App extends React.Component {
  render() {
    return (
      <Router history={createBrowserHistory()}>
        <Route path="/" component={Body}>
          <IndexRoute component={Home} />
          <Route path="login" component={Login} />
          <Route path="sign-up" component={Registration} />

          <Route path="brainstorms" component={Brainstorms}>
            <IndexRoute component={BrainstormList} />
            <Route path="detail/:brainstorm_id" component={BrainstormDetail} />
          </Route>
        </Route>
      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));
