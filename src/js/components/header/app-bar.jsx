import React from 'react';
import {
  Nav,
  Navbar,
  NavBrand
} from 'react-bootstrap';
import { IndexLink, Link } from 'react-router';
import NavLink from '../nav-link.jsx';

export default class AppBar extends React.Component {
  render() {
    return (
      <Navbar>
        <NavBrand>
          <IndexLink to="/" activeClassName="active">Cadence Rocks</IndexLink>
        </NavBrand>
        <Nav>
          <NavLink to="/brainstorms">
            Brainstorms
          </NavLink>
        </Nav>
      </Navbar>
    );
  }
};
