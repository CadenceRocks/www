import React from 'react';
import {
  Col,
  Grid,
  PageHeader,
  Row
} from 'react-bootstrap';

export default class BrainstormDetail extends React.Component {
  render() {
    return (
      <Grid>
        <Row>
          <Col xs={12}>
            <PageHeader>Brainstorm {this.props.params.brainstorm_id}</PageHeader>
          </Col>
        </Row>

        <Row>
          <Col xs={12}>
            <p>These are the details for brainstorm id: {this.props.params.brainstorm_id}</p>
          </Col>
        </Row>
      </Grid>
    );
  }
}
