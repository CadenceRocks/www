import React from 'react';
import {
  Col,
  Grid,
  ListGroup,
  ListGroupItem,
  PageHeader,
  Row
} from 'react-bootstrap';
import {Link} from 'react-router';

export default class BrainstormList extends React.Component {
  render() {
    let brainstorms = [1, 2, 3, 4, 5].map(it => {
      return (
        <ListGroupItem key={it}>
          <Link to={'/brainstorms/detail/' + it}>
            Brainstorm {it}
          </Link>
        </ListGroupItem>
      );
    });

    return (
      <Grid>
        <PageHeader>
          Brainstorms
        </PageHeader>
        <Row>
          <Col xs={12}>
            <ListGroup>
              {brainstorms}
            </ListGroup>
          </Col>
        </Row>
      </Grid>
    );
  }
}
