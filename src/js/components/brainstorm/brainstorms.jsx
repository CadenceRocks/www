import React from 'react';

export default class Brainstorms extends React.Component {
  render() {
    return (
      <div>{this.props.children}</div>
    );
  }
}
