import React from 'react';
import {
  Button,
  Col,
  Grid,
  Input,
  Panel,
  Row
} from 'react-bootstrap';

export default class Registration extends React.Component {
  render() {
    return (
      <Grid>
        <Row>
          <Col xs={6} xsOffset={3}>
            <Panel header="Sign up">
              <Input
                type="text"
                ref="email"
                placeholder="Email"/>

              <Input
                type="password"
                ref="password"
                placeholder="Password"/>

              <Input
                type="password"
                ref="password2"
                placeholder="Repeat password"/>

              <Button bsStyle="primary" block>Sign up</Button>
            </Panel>
          </Col>
        </Row>
      </Grid>
    );
  }
}