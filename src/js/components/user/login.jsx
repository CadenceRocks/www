import React from 'react';
import {
  Button,
  Col,
  Grid,
  Input,
  Panel,
  Row
} from 'react-bootstrap';

export default class Login extends React.Component {
  render() {
    return (
      <Grid>
        <Row>
          <Col xs={6} xsOffset={3}>
            <Panel header="Login">
              <Input
                type="text"
                ref="email"
                placeholder="Email" />

              <Input
                type="password"
                ref="password"
                placeholder="Password" />

              <Button bsStyle="primary" block>Login</Button>
            </Panel>
          </Col>
        </Row>
      </Grid>
    );
  }
}