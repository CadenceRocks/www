'use strict';

import React from 'react';
import {
  Col,
  Grid,
  Jumbotron,
  Row
} from 'react-bootstrap';
import {Link} from 'react-router';

export default class Home extends React.Component {
  render() {
    return (
      <Grid>
        <Row>
          <Col md={12}>
            <Jumbotron>
              <h1>Your Ideas Matter</h1>
              <p>
                Cadence is an application for empowering teams to
                identify difficult problems and collaborate on innovative solutions.
              </p>
              <p>
                <Link to='/sign-up'>Sign up</Link> or <Link to='/login'>login</Link> now!
              </p>
            </Jumbotron>
          </Col>
        </Row>
      </Grid>
    );
  }
}
