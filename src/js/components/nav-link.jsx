import React from 'react';
import {Link} from 'react-router';

export default class NavLink extends React.Component {
  render() {
    return (
      <li>
        <Link to={this.props.to} activeClassName="active">{this.props.children}</Link>
      </li>
    );
  }
}